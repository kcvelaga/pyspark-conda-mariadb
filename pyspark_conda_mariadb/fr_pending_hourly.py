import argparse
import json
import subprocess
import sys
import urllib.request

import dns.resolver
from pyspark.sql import SparkSession
from pyspark.sql import functions as F
from pyspark.sql.types import DateType, IntegerType, LongType, StringType


def parse_args(args):
    parser = argparse.ArgumentParser(
        description="Calculates pending flagged revisions to be reviewed."
    )
    parser.add_argument(
        "--wiki_db",
        help="wiki_db to calculate the pending flagged revisions for.",
        required=True,
    )
    parser.add_argument(
        "--destination_table",
        help="destination table to update the calculated metrics with.",
        required=True,
    )

    return parser.parse_args(args)


def fetch_analytics_mariadb_password():
    result = subprocess.run(
        """sudo -u analytics-product kerberos-run-command analytics-product \
        hdfs dfs -cat /user/analytics-product/mysql-analytics-research-client-pw.txt""",
        shell=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        universal_newlines=True,
    )

    pw = result.stdout.strip()

    return pw


# source: https://w.wiki/AFBy (from @xcollazo)
def resolve_shard_for_wikidb(wikidb, datacenter="eqiad"):
    url = f"https://noc.wikimedia.org/db.php?dc={datacenter}&format=json"
    with urllib.request.urlopen(url) as response:
        data = json.loads(response.read().decode())
        for shard, info in data.items():
            if wikidb in info["dbs"]:
                return shard
    return "s3"


# source: https://w.wiki/AFBy (from @xcollazo)
def get_mariadb_host_port_for_wikidb(wikidb):
    shard = resolve_shard_for_wikidb(wikidb)
    answers = dns.resolver.resolve("_" + shard + "-analytics._tcp.eqiad.wmnet", "SRV")
    host, port = str(answers[0].target), answers[0].port
    return (host[:-1], port)


dtypes_mapping = {
    "wiki_db": StringType(),
    "date": DateType(),
    "hour": IntegerType(),
    "pending_frevs": LongType(),
    "avg_elapsed_secs": LongType(),
}


def sparkDF_convert_dtypes(df, mapping=dtypes_mapping):
    for col, dtype in mapping.items():
        df = df.withColumn(col, df[col].cast(dtype))
    return df


def load_query():
    sql_file = (
        "https://gitlab.wikimedia.org/kcvelaga/data-pipelines/-/raw/"
        "fr_pending/flaggedrevs/fr_pending_hourly/calculate_fr_pending_hourly.sql"
    )

    with urllib.request.urlopen(sql_file) as response:
        query = response.read().decode()

    return query


def calculate_pending_frevs(spark_session, wiki_db, host, port, query, replicas_pw):

    jdbc = (
        spark_session.read.format("jdbc")
        .option("driver", "com.mysql.cj.jdbc.Driver")
        .option("numPartitions", 1)
        .option("url", f"jdbc:mysql://{host}:{port}/{wiki_db}")
        .option("query", query.format(wiki_db=wiki_db))
        .option("user", "research")
        .option("password", replicas_pw)
        .load()
    )

    return jdbc


def update_fr_pending_hourly(spark_session, df, destination_table) -> None:

    df = sparkDF_convert_dtypes(df)

    partition_values = (
        df.select(F.to_date("date").alias("date"), "hour", "wiki_db")
        .distinct()
        .collect()[0]
    )

    spark_session.sql(
        f"""
        DELETE FROM {destination_table}
        WHERE
            date = '{partition_values['date'].strftime('%Y-%m-%d')}'
            AND hour = {partition_values['hour']}
            AND wiki_db = '{partition_values['wiki_db']}'
    """
    )

    df.write.format("iceberg").mode("append").saveAsTable(destination_table)


def main() -> None:

    args = parse_args(sys.argv[1:])

    spark_config = {
        "spark.driver.memory": "2g",
        "spark.dynamicAllocation.maxExecutors": 64,
        "spark.executor.memory": "8g",
        "spark.executor.cores": 4,
        "spark.sql.shuffle.partitions": 256,
        "spark.jars.packages": "org.apache.iceberg:iceberg-spark-runtime-3.1_2.12:1.2.1,com.mysql:mysql-connector-j:8.2.0",
        "spark.jars.ivySettings": "/etc/maven/ivysettings.xml",
    }
    session_builder = SparkSession.builder
    for k, v in spark_config.items():
        session_builder = session_builder.config(k, v)
    spark_session = session_builder.getOrCreate()
    # spark_session = SparkSession.builder.getOrCreate()

    (host, port) = get_mariadb_host_port_for_wikidb(args.wiki_db)
    pw = fetch_analytics_mariadb_password()
    query = load_query()

    pending_frevs = calculate_pending_frevs(
        spark_session, args.wiki_db, host, port, query, pw
    )

    update_fr_pending_hourly(spark_session, pending_frevs, args.destination_table)

    spark_session.stop()


if __name__ == "__main__":
    main()
