from pyspark_conda_mariadb.fr_pending_hourly import parse_args 

def test_parse_args():
    test_args = [
        '--wiki_db', 'dewiki',
        '--destination_table', 'kcvelaga.fr_pending_hourly'
    ]
    parsed = parse_args(test_args)
    print(parsed)

    assert parsed.wiki_db == 'dewiki'
    assert parsed.destination_table == 'kcvelaga.fr_pending_hourly'